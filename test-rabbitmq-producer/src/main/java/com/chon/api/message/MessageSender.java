package com.chon.api.message;

import com.chon.api.dto.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
public class MessageSender {

    @Value("${mq.topic-name}")
    private String topicQueueName;

    @Value("${mq.topic-name2}")
    private String topicQueueName2;

    @Value("${mq.fanout-name}")
    private String fanoutQueueName;

    @Autowired
    private TopicExchange topicExchange;

    @Autowired
    private FanoutExchange fanoutExchange;

    @Autowired
    private DirectExchange directExchange;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendTopicCustomer(Customer customer) {
        log.info("Send to MQ {}",topicQueueName);
        rabbitTemplate.convertAndSend(topicExchange.getName(), topicQueueName, customer);
        log.info("Send to MQ {}",topicQueueName2);
        rabbitTemplate.convertAndSend(topicExchange.getName(), topicQueueName2, customer);
    }

    public void sendFanoutCustomer(Customer customer) {
        log.info("Send to MQ {}",fanoutQueueName);
        rabbitTemplate.convertAndSend(fanoutExchange.getName(), fanoutQueueName, customer);
    }

}
