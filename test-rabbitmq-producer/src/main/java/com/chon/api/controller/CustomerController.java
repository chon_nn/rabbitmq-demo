package com.chon.api.controller;

import com.chon.api.dto.Customer;
import com.chon.api.message.MessageSender;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
public class CustomerController {

    private final MessageSender messageSender;

    @PostMapping("/topic")
    public Customer postTopicCustomer(@RequestBody Customer body) {
        messageSender.sendTopicCustomer(body);
        return body;
    }

    @PostMapping("/fanout")
    public Customer postFanoutCustomer(@RequestBody Customer body) {
        messageSender.sendFanoutCustomer(body);
        return body;
    }

    @PostMapping("/direct")
    public Customer postDirectCustomer(@RequestBody Customer body) {
        return body;
    }

}
