package com.chon.api.config;

import com.chon.api.message.MessageSender;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProducerMessageConfig {
    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange("topic.exchange",true,false);
    }

    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange("fanout.exchange",true,false);
    }

    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange("direct.exchange",true,false);
    }

    @Bean
    public MessageSender sender() {
        return new MessageSender();
    }

}