package com.chon.api.message;

import com.chon.api.dto.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

@RabbitListener(queues = "${mq.topic-name}")
@Slf4j
public class TopicMessageReceiver {

    @RabbitHandler
    public void receive(Customer customer) throws InterruptedException {

        log.info("Message from MQ: {}",String.valueOf(customer));
        Thread.sleep(5000);
    }

}
