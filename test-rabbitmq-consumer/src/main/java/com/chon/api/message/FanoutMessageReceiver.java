package com.chon.api.message;

import com.chon.api.dto.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

@RabbitListener(queues = "${mq.fanout-name}")
@Slf4j
public class FanoutMessageReceiver {

    @RabbitHandler
    public void receive(Customer customer) {
        log.info("Message from fanout MQ: {}",String.valueOf(customer));
    }

}
