package com.chon.api.config;

import com.chon.api.message.FanoutMessageReceiver;
import com.chon.api.message.TopicMessageReceiver;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConsumerMessageConfig {

    @Value("${mq.topic-name}")
    private String topicQueueName;

    @Value("${mq.fanout-name}")
    private String fanoutQueueName;

    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange("topic.exchange");
    }

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("direct.exchange");
    }

    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("fanout.exchange");
    }


    @Bean
    public Queue fanoutQueue() {
        return new Queue(fanoutQueueName);
    }

    @Bean
    public Binding fanoutBinding(FanoutExchange fanoutExchange, Queue fanoutQueue) {
        return BindingBuilder.bind(fanoutQueue).to(fanoutExchange);
    }

    @Bean
    public Queue topicQueue() {
        return new Queue(topicQueueName,true,true,false);
    }

    @Bean
    public Binding topicBinding(TopicExchange topicExchange, Queue topicQueue) {
        return BindingBuilder.bind(topicQueue).to(topicExchange).with(topicQueueName);
    }

    @Bean
    public TopicMessageReceiver topicMessageReceiver() {
        return new TopicMessageReceiver();
    }

    @Bean
    public FanoutMessageReceiver fanoutMessageReceiver() {
        return new FanoutMessageReceiver();
    }

}